//
//  tohbehAppDelegate.h
//  tohbeh
//
//  Created by misael vazquez mis on 19/02/14.
//  Copyright (c) 2014 MisaelVazquez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tohbehAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
