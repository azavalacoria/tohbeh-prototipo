//
//  tohbehViewController.h
//  tohbeh
//
//  Created by misael vazquez mis on 19/02/14.
//  Copyright (c) 2014 MisaelVazquez. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tohbehViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource>

@property (weak, nonatomic) IBOutlet UIPickerView *mypaquete;

@property (strong, nonatomic) NSArray *mispaquetes;

@property (weak, nonatomic) IBOutlet UIPickerView *Menores;

@property (strong, nonatomic) NSArray *menoresedad;

@end
